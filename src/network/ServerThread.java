package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerThread extends Thread{

    private Server server;
    private Socket client1;
    private Socket client2;

    private BufferedReader fromClient1, fromClient2;
    private PrintWriter toClient1, toClient2;

    public ServerThread(Server server, Socket client1, Socket client2) {
        this.server = server;
        this.client1 = client1;
        this.client2 = client2;

        try{
            this.fromClient1 = new BufferedReader(new InputStreamReader(this.client1.getInputStream()));
            this.fromClient2 = new BufferedReader(new InputStreamReader(this.client2.getInputStream()));
            this.toClient1 = new PrintWriter(this.client1.getOutputStream());
            this.toClient2 = new PrintWriter(this.client2.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String message;

        try{

            // send players who is on turn
            toClient1.println("true");
            toClient1.flush();
            toClient2.println("false");
            toClient2.flush();

            while(true){
                message = fromClient1.readLine();
                System.err.println("potez igraca 1: " + message);
                toClient2.println(message);
                toClient2.flush();

                if(message.startsWith("exit")){
                    // write who is win
                    break;
                }

                message = fromClient2.readLine();
                System.err.println("potez igraca 2: " + message);
                toClient1.println(message);
                toClient1.flush();

                if(message.startsWith("exit")){
                    // write who is win
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                this.client1.close();
                this.client2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }









}
