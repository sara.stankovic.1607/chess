package network;

import game.Game;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

    private final String hostname;
    private final int port;

    private ClientThread clientThread;

    public Client(String hostname, int port){
        this.port = port;
        this.hostname = hostname;
    }

    public void start(){
        try(Socket socket = new Socket(hostname, port)){

            System.err.println("Connected to server @" + hostname);

            clientThread = new ClientThread(socket);

            clientThread.start();
            clientThread.join();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ClientThread getClientThread() {
        return clientThread;
    }
}
