package network;

import game.Field;
import game.Game;
import javafx.application.Platform;
import javafx.event.EventHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ClientThread extends Thread implements EventHandler<MoveEvent> {

    private BufferedReader fromServer;
    private PrintWriter toServer;

    private boolean onTurn;

    private BlockingQueue<String> moveQueue = new LinkedBlockingQueue<>();


    public ClientThread(Socket socket) {

        for(int i=0; i<2; i++)
            for(int j=0; j<8; j++)
                Game.getBoard().getField(i,j).getPiece().setMoveEventHandler(this);

        for(int i=6; i<8; i++)
            for(int j=0; j<8; j++)
                Game.getBoard().getField(i,j).getPiece().setMoveEventHandler(this);

        try{
            this.fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.toServer = new PrintWriter(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        String message;

        try {

            // set onTurn
            message = fromServer.readLine();
            onTurn = message.equals("true");

            while(true){


                if(onTurn){ // ako je na potezu, cekamo da se odigra potez i to saljemo na server
                    String move = getMoveFromQueue();
                    System.err.println("my move: " + move);
                    if(move.startsWith("exit"))
                        break;
                    sendMessage(move);
                } else{
                    message = fromServer.readLine();
                    System.err.println("opponent's move: " + message);

                    Game.movePiece(message);

                    Platform.runLater(() -> {
                        Game.stage.show();
                        for(int i=0; i<8; i++)
                            for(int j=0; j<8; j++)
                                Game.getBoard().getField(i,j).refreshField();
                    });

                    if(message.startsWith("exit"))
                        break;
                }
                onTurn = !onTurn;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            System.err.println("!!! getMoveFromQueue()");
            e.printStackTrace();
        }

        // napravi se potez
        // napravi se poruka o potezu (potez ili kraj igre)
        // prosledi se poruka serveru

        // procita se poruka sa servera o potezu suprotnog igraca
        // pomeri se figura koja treba (ili kraj igre)

    }


    // implementacija posmatraca za potez
    @Override
    public void handle(MoveEvent moveEvent) {

        String move = moveEvent.getMove();
        try {
            moveQueue.put(move);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getMoveFromQueue() throws InterruptedException {
        // blokira se nit dok ne dodje potez u queue
        return moveQueue.take();
    }

    private void sendMessage(String move) {
        toServer.println(move);
        toServer.flush();
    }

    public boolean isOnTurn(){
        return onTurn;
    }

}
