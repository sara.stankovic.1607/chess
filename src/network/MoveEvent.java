package network;

import javafx.event.Event;
import javafx.event.EventType;

public class MoveEvent extends Event {

    private static final EventType<? extends Event> MOVE_EVENT_TYPE = new EventType<>(Event.ANY, "MOVE_EVENT");
    private String move;

    public MoveEvent(String move) {
        super(MOVE_EVENT_TYPE);
        this.move = move;
    }

    public String getMove() {
        return move;
    }
}

