package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server{

    public static final int PORT = 12345;

    public static void main(String[] args) {
        Server server = new Server(PORT);
        server.start();
    }

    private int port;
    private Socket clientSocket1;
    private Socket clientSocket2;

    public Server(int port){
        this.port = port;
    }

    private void start() {
        try(ServerSocket serverSocket = new ServerSocket(port)) {

            System.err.println("Server listeing on port: " + port);

            clientSocket1 = serverSocket.accept();
            System.err.println("Client1 connected");

            clientSocket2 = serverSocket.accept();
            System.err.println("Client2 connected");

            ServerThread serverThread = new ServerThread(this, clientSocket1, clientSocket2);
            serverThread.start();
            serverThread.join();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            System.err.println("Server errored");
        }
    }


}
