package pieces;

import game.*;
import javafx.scene.image.Image;

public class Queen extends Piece{
    public Queen(Color color) {
        super(color);
    }

    private static Image blackImage = new Image("/black_queen.png");
    private static Image whiteImage = new Image("/white_queen.png");

    @Override
    public Image getImage() {
        if(isBlack())
            return blackImage;
        else
            return whiteImage;
    }

    @Override
    public String getType() {
        return "queen";
    }

    @Override
    public boolean move(Field start, Field end, Board board) {

        if(Game.getGameMode().equals("online") && !Game.getClient().getClientThread().isOnTurn())
            return false;

        if(!end.isEmpty() && end.getPiece().getColor() == this.getColor())
            return false;

        int x1 = start.getX(), x2 = end.getX();
        int y1 = start.getY(), y2 = end.getY();

        if (!(x1 == x2 || y1 == y2 || Math.abs(x1 - x2) == Math.abs(y1 - y2)))
            return false;

        if(x1 == x2){
            int step = y1 < y2 ? 1 : -1;
            for(int i=y1+step; i+step != y2 && i != y2; i += step){
                Field f = board.getField(x1, i);
                if(!f.isEmpty())
                    return false;
            }
        } else if(y1 == y2){
            int step = x1 < x2 ? 1 : -1;
            for(int i=x1+step; i+step != x2 && i != x2; i += step){
                Field f = board.getField(i, y1);
                if(!f.isEmpty())
                    return false;
            }
        } else{
            int step1 = x1 < x2 ? 1 : -1;
            int step2 = y1 < y2 ? 1 : -1;
            for(int i=x1+step1, j=y1+step2; i+step1 != x2 && i != x2; i+=step1, j+=step2){
                Field f = board.getField(i, j);
                if(!f.isEmpty())
                    return false;
            }
        }

        // make move and return true
        if(!end.isEmpty())
            end.getPiece().kill();

        end.setPiece(this);
        start.setPiece(null);

        start.refreshField();
        end.refreshField();

        if(Game.getGameMode().equals("online"))
            fireMoveEvent(Game.createMoveString(start, end));

        return true;
    }
}
