package pieces;

import game.*;
import javafx.scene.image.Image;

public class Pawn extends Piece{
    public Pawn(Color color) {
        super(color);
    }

    private static Image blackImage = new Image("/black_pawn.png");
    private static Image whiteImage = new Image("/white_pawn.png");

    @Override
    public Image getImage() {
        if(isBlack())
            return blackImage;
        else
            return whiteImage;
    }

    @Override
    public boolean move(Field start, Field end, Board board) {

        if(Game.getGameMode().equals("online") && !Game.getClient().getClientThread().isOnTurn())
            return false;

        int step = this.isBlack() ? -1 : 1;

        int x1 = start.getX(), x2 = end.getX();
        int y1 = start.getY(), y2 = end.getY();

        // one step forward and there is no figure
        boolean cond1 = x1+step == x2 && y1 == y2 && end.isEmpty();
        // two steps forward if it is first move
        boolean cond2 = x1 == (this.isBlack() ? 6 : 1);
        cond2 = cond2 && x1+step*2 == x2 && y1 == y2 && end.isEmpty();
        // one step diagonaly if it kills someone
        boolean cond3 = x1+step == x2 && Math.abs(y1-y2) == 1;
        cond3 = cond3 && !end.isEmpty() && end.getPiece().getColor() != this.getColor();

        if(!cond1 && !cond2 && !cond3)
            return false;

        if(!end.isEmpty())
            end.getPiece().kill();

        // make move
        start.setPiece(null);
        end.setPiece(this); // ???

        // promotion
        if(x2 == (this.isBlack() ? 0 : 7)){
            Queen q = new Queen(this.getColor());
            end.setPiece(q);
            //? this.kill();
        }

        start.refreshField();
        end.refreshField();


        // inicira se dogadjaj
        if(Game.getGameMode().equals("online"))
            fireMoveEvent(Game.createMoveString(start, end));

        return true;
    }

    @Override
    public String getType() {
        return "pawn";
    }
}
