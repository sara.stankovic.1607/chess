package pieces;

import game.*;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import network.MoveEvent;

public abstract class Piece {
    private Color color;
    private boolean live;

    private EventHandler<MoveEvent> moveEventHandler;

    public Piece(Color color){
        this.color = color;
        live = true;

        moveEventHandler = null;
    }

    public void setMoveEventHandler(EventHandler<MoveEvent> moveEventHandler) {
        this.moveEventHandler = moveEventHandler;
    }

    public void fireMoveEvent(String move){
        if(moveEventHandler != null){
            MoveEvent moveEvent = new MoveEvent(move);
            moveEventHandler.handle(moveEvent);
        }
    }

    public void kill(){
        live = false;
    }

    public Color getColor() {
        return color;
    }

    public boolean isBlack(){
        return color == Color.BLACK;
    }

    public boolean isKilled(){
        return !live;
    }

    // make the move if it's possible and return is it make a move
    public abstract boolean move(Field start, Field end, Board board);

    public abstract Image getImage();

    public abstract String getType();
}
