package pieces;

import game.*;
import javafx.scene.image.Image;

public class King extends Piece{
    public King(Color color) {
        super(color);
    }

    private static Image blackImage = new Image("/black_king_1.png");
    private static Image whiteImage = new Image("/white_king.png");

    @Override
    public Image getImage() {
        if(isBlack())
            return blackImage;
        else
            return whiteImage;
    }

    @Override
    public boolean move(Field start, Field end, Board board) {

        if(Game.getGameMode().equals("online") && !Game.getClient().getClientThread().isOnTurn())
            return false;

        // can't move to field with the same piece color
        if(!end.isEmpty() && end.getPiece().getColor() == this.getColor())
            return false;

        // todo: rokada

        int x = Math.abs(start.getX()-end.getX());
        int y = Math.abs(start.getY()-end.getY());

        if(!(x<=1 && y <= 1))
            return false;

        if(board.isAttacked(this, end)) // check is it chess
            return false;

        if(!end.isEmpty())
            end.getPiece().kill();

        // TODO: write if you kill somone
        end.setPiece(this);
        start.setPiece(null);


        start.refreshField();
        end.refreshField();

        if(Game.getGameMode().equals("online"))
            fireMoveEvent(Game.createMoveString(start, end));

        return true;
    }

    @Override
    public String getType() {
        return "king";
    }
}