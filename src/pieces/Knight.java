package pieces;

import game.*;
import javafx.scene.image.Image;

public class Knight extends Piece{
    public Knight(Color color) {
        super(color);
    }

    private static Image blackImage = new Image("/black_knight.png");
    private static Image whiteImage = new Image("/white_knight.png");

    @Override
    public Image getImage() {
        if(isBlack())
            return blackImage;
        else
            return whiteImage;
    }

    @Override
    public String getType() {
        return "knight";
    }

    @Override
    public boolean move(Field start, Field end, Board board) {

        if(Game.getGameMode().equals("online") && !Game.getClient().getClientThread().isOnTurn())
            return false;

        // can't move to field with the same piece color
        if(!end.isEmpty() && end.getPiece().getColor() == this.getColor())
            return false;

        int x1 = start.getX(), x2 = end.getX();
        int y1 = start.getY(), y2 = end.getY();

        int diff1 = Math.abs(x1-x2);
        int diff2 = Math.abs(y1-y2);

        if (!((diff1 == 2 && diff2 == 1) || (diff1 == 1 && diff2 == 2)))
            return false;

        if(!end.isEmpty())
            end.getPiece().kill();

        start.setPiece(null);
        end.setPiece(this);

        start.refreshField();
        end.refreshField();

        if(Game.getGameMode().equals("online"))
            fireMoveEvent(Game.createMoveString(start, end));

        return true;
    }
}
