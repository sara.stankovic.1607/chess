package pieces;

import game.*;
import javafx.scene.image.Image;

public class Rook extends Piece{
    public Rook(Color color) {
        super(color);
    }

    private static Image blackImage = new Image("/black_rook.png");
    private static Image whiteImage = new Image("/white_rook.png");

    @Override
    public Image getImage() {
        if(isBlack())
            return blackImage;
        else
            return whiteImage;
    }

    @Override
    public String getType() {
        return "rook";
    }

    @Override
    public boolean move(Field start, Field end, Board board) {

        if(Game.getGameMode().equals("online") && !Game.getClient().getClientThread().isOnTurn())
            return false;

        if(!end.isEmpty() && end.getPiece().getColor() == this.getColor())
            return false;

        int x1 = start.getX(), x2 = end.getX();
        int y1 = start.getY(), y2 = end.getY();

        if(!(y1 == y2 || x1 == x2))
            return false;


        if(x1 != x2){
            int step = x1 < x2 ? 1 : -1;
            for(int i=x1+step; i+step != x2 && i != x2; i += step){
                Field f = board.getField(i, y1);
                if(!f.isEmpty())
                    return false;
            }
        } else{
            int step = y1 < y2 ? 1 : -1;
            for(int i=y1+step; i+step != y2 && i != y2; i += step){
                Field f = board.getField(x1, i);
                if(!f.isEmpty())
                    return false;
            }
        }

        if(!end.isEmpty())
            end.getPiece().kill();

        end.setPiece(this);
        start.setPiece(null);
        // todo: check is it checkmate

        start.refreshField();
        end.refreshField();

        if(Game.getGameMode().equals("online"))
            fireMoveEvent(Game.createMoveString(start, end));

        return true;
    }
}
