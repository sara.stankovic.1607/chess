import game.Game;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Chess");

        stage.setMinHeight(500);
        stage.setMinWidth(500);

        createFirstStage(stage);
    }

    private void createFirstStage(Stage stage) {
        
        Game.stage = stage;

        Label l = new Label("Choose the game mode");

        RadioButton rbOnline = new RadioButton("online");
        RadioButton rbOffline = new RadioButton("offline");
        //TODO: RadioButton rbAi = new RadioButton("ai");

        ToggleGroup toggleGroup = new ToggleGroup();

        rbOffline.setToggleGroup(toggleGroup);
        rbOnline.setToggleGroup(toggleGroup);

        rbOffline.setSelected(true);

        Button btStart = new Button("start game");

        btStart.setOnAction(e -> {
            if(rbOffline.isSelected())
                startOfflineGame(stage);
            else if(rbOnline.isSelected())
                startOnlineGame(stage);
        });

        VBox root = new VBox(10, l, rbOffline, rbOnline, btStart);

        stage.setScene(new Scene(root));
        stage.show();
    }

    private void startOfflineGame(Stage stage) {
        VBox root = new VBox();

        Game.start("offline");

        HBox hbox = new HBox();
        Button btNewGame = new Button("new"); // TODO: picture


        btNewGame.setOnAction(e -> {
            Game.startNewGame("offline");
        });

        stage.setOnCloseRequest(e -> {
            Game.save();
        });

        hbox.getChildren().addAll(btNewGame);
        root.getChildren().addAll(Game.getBoard(), hbox);

        stage.setScene(new Scene(root));
        stage.show();
    }

    private void startOnlineGame(Stage stage) {



        new Thread(() -> {

            Game.startNewGame("online");

            Platform.runLater(() -> {

                stage.setScene(new Scene(Game.getBoard()));
                stage.show();
                for(int i=0; i<8; i++)
                    for(int j=0; j<8; j++)
                        Game.getBoard().getField(i,j).refreshField();
            });

            Game.createClient();

            while (true) {
                Platform.runLater(() -> {
                    for(int i=0; i<8; i++)
                        for(int j=0; j<8; j++)
                            Game.getBoard().getField(i,j).refreshField();
                });

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();



    }



}
