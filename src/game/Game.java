package game;

import javafx.stage.Stage;
import network.Client;
import network.Server;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import pieces.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Game{
    private static Board board;
    public static Stage stage;

    private static Color onTurn;

    private static Field fromField;
    private static Field toField;

    private static String gameMode;
    private static Client client;

    private Game(){ }


    public static void start(String mode) {

        gameMode = mode;

        fromField = null;
        toField = null;

        List<Field> fields = load();
        createBoard(fields);
    }

    public static void startNewGame(String mode) {

        gameMode = mode;
        onTurn = Color.WHITE;

        fromField = null;
        toField = null;

        createBoard(null);

        /*if(gameMode.equals("online")) {
            client = new Client("localhost", Server.PORT);
            client.start();
        }*/

    }

    public static void createClient(){
        client = new Client("localhost", Server.PORT);
        client.start();
    }

    public static String getGameMode() {
        return gameMode;
    }

    public static Client getClient() {
        return client;
    }

    public static void createBoard(List<Field> fields) {
        if(Game.getGameMode().equals("offline")){
            if (fields == null) {
                board.getChildren().clear();
                board.resetBoard();
            }
            else
                board = new Board(fields);
        } else{
            board = new Board();
        }

        for(int i=0; i<8; i++)
            board.addRow(i, board.getFields()[i]);

    }

    public static Board getBoard() {
        return board;
    }

    public static Field getToField() {
        return toField;
    }

    public static Field getFromField() {
        return fromField;
    }


    public static void nextTurn() {
        onTurn = onTurn == Color.WHITE ? Color.BLACK : Color.WHITE;
    }

    public static Color getOnTurn() {
        return onTurn;
    }

    public static void setFromField(Field f) {
        if(fromField != null)
            fromField.setColor(fromField.getColor().toString());

        Game.fromField = f;
        if(fromField != null)
            fromField.setColor("ffff00");
    }

    public static void setToField(Field toField) {
        Game.toField = toField;
    }

    public static void save() {

        JSONObject json = new JSONObject();
        json.put("turn", getOnTurn().toString());

        JSONArray pieceList = new JSONArray();
        for(int i=0; i<8; i++)
            for(int j=0; j<8; j++){
                Field f = board.getField(i, j);
                if(!f.isEmpty()){
                    JSONObject o = new JSONObject();
                    o.put("i", i);
                    o.put("j", j);
                    o.put("color", f.getPiece().getColor().toString());
                    o.put("type", f.getPiece().getType());
                    pieceList.add(o);
                }
            }

        json.put("pieces", pieceList);
        

        try(FileWriter fw = new FileWriter("saved_game.json")){
            //fw.write(pieceList.toJSONString());
            fw.write(json.toJSONString());
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public static List<Field> load(){
        List<Field> fields = new LinkedList<>();

        JSONParser parser = new JSONParser();

        try(FileReader reader = new FileReader("saved_game.json")){

            JSONObject object = (JSONObject) parser.parse(reader);

            JSONArray array = (JSONArray) object.get("pieces");

            array.forEach(p -> {
                Field f = parsePiece((JSONObject) p);
                fields.add(f);
            });

            onTurn = Color.fromString((String)object.get("turn"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return fields;
    }

    private static Field parsePiece(JSONObject p) {

        Color color = Color.fromString((String) p.get("color"));
        int i = (int) (long) p.get("i");
        int j = (int) (long) p.get("j");
        String type = (String) p.get("type");

        Piece piece = null;

        switch(type){
            case "rook" :
                piece = new Rook(color);
                break;
            case "queen" :
                piece = new Queen(color);
                break;
            case "king" :
                piece = new King(color);
                break;
            case "bishop" :
                piece = new Bishop(color);
                break;
            case "pawn" :
                piece = new Pawn(color);
                break;
            case "knight" :
                piece = new Knight(color);
                break;
        }

        return new Field(i, j, piece);
    }

    /*public static void sendMoveToServer(Field start, Field end) {
        String move = Game.createMoveString(start, end);
        client.sendMessage(move);
    }*/

    public static String createMoveString(Field start, Field end) {
        int x1 = start.getX(), x2 = end.getX(), y1 = start.getY(), y2 = end.getY();
        return x1 + " " + y1 + " " + x2 + " " + y2; // TODO
    }

    public static void movePiece(String move) {

        // parse String move
        // 1 2 4 0
        Scanner sc = new Scanner(move);
        if(move.startsWith("exit"))
            return;

        int x1 = sc.nextInt(), y1 = sc.nextInt(), x2 = sc.nextInt(), y2 = sc.nextInt();

        Piece piece = Game.getBoard().getField(x1, y1).getPiece();
        Game.getBoard().getField(x1, y1).setPiece(null);
        Game.getBoard().getField(x2, y2).setPiece(piece);

        Game.nextTurn(); // ??

    }
}
