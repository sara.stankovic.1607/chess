package game;

import javafx.scene.layout.GridPane;
import pieces.*;

import java.util.List;

public class Board extends GridPane {
    private Field[][] fields;

    public Board(){
        resetBoard();
    }

    public Board(List<Field> usedFields) {
        fields = new Field[8][8];

        for(int i=0; i<8; i++)
            for(int j=0; j<8; j++)
                fields[i][j] = new Field(i,j);

        for(Field f : usedFields){
            int x = f.getX(), y = f.getY();
            fields[x][y] = f;
        }
    }

    public void resetBoard() {

        fields = new Field[8][8];

        // 1a-1h
        fields[0][0] = new Field(0,0, new Rook(Color.WHITE));
        fields[0][1] = new Field(0,1, new Knight(Color.WHITE));
        fields[0][2] = new Field(0,2, new Bishop(Color.WHITE));
        fields[0][3] = new Field(0,3, new Queen(Color.WHITE));
        fields[0][4] = new Field(0,4, new King(Color.WHITE));
        fields[0][5] = new Field(0,5, new Bishop(Color.WHITE));
        fields[0][6] = new Field(0,6, new Knight(Color.WHITE));
        fields[0][7] = new Field(0,7, new Rook(Color.WHITE));

        // 2a-2h
        for(int i=0; i<8; i++)
            fields[1][i] = new Field(1,i, new Pawn(Color.WHITE));

        // 3a-6h
        for(int i=2; i<=5; i++)
            for(int j=0; j<8; j++)
                fields[i][j] = new Field(i,j);

        // 7a-7h
        for(int i=0; i<8; i++)
            fields[6][i] = new Field(6,i, new Pawn(Color.BLACK));

        // 8a-8h
        fields[7][0] = new Field(7,0, new Rook(Color.BLACK));
        fields[7][1] = new Field(7,1, new Knight(Color.BLACK));
        fields[7][2] = new Field(7,2, new Bishop(Color.BLACK));
        fields[7][3] = new Field(7,3, new Queen(Color.BLACK));
        fields[7][4] = new Field(7,4, new King(Color.BLACK));
        fields[7][5] = new Field(7,5, new Bishop(Color.BLACK));
        fields[7][6] = new Field(7,6, new Knight(Color.BLACK));
        fields[7][7] = new Field(7,7, new Rook(Color.BLACK));

    }

    public Field[][] getFields() {
        return fields;
    }

    public boolean isAttacked(Piece piece, Field field){
        // TODO
        return false;
    }

    public Field getField(int x, int y){
        return fields[x][y]; // todo: check and throw exeption
    }
}
