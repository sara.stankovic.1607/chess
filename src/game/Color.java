package game;

public enum Color {
    WHITE, BLACK;

    @Override
    public String toString() {
        return this == Color.WHITE ? "white" : "black";
    }

    public static Color fromString(String s){
        return s.equals("white") ? Color.WHITE : Color.BLACK;
    }
}
