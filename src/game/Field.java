package game;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import pieces.Piece;

public class Field extends Button {
    private int x;
    private int y;
    private Piece piece;
    private Color color;

    private ImageView view;

    public Field(int x, int y, Piece piece){
        this.x = x;
        this.y = y;
        this.piece = piece;
        this.color = (x+y)%2==1 ? Color.WHITE : Color.BLACK;

        setButtonDesign();

        this.setOnAction(e -> onClick());
    }

    public Field(int x, int y){
        this(x,y,null);
    }

    private void onClick() {
        // ako je kliknuto na prazno polje
        // 1. ako prethodno vec nije izabrano polje, nista
        // 2. ako je vec izabrano -> pomeri igraca ako je moguce

        // ako nije prazno polje
        // 2. prethodno vec izabrano, pokusaj da napadnes
        // 1. ako nije izabrano, izaberi ako je odgovarajuce boje

        if(Game.getGameMode().equals("online") && !Game.getClient().getClientThread().isOnTurn())
            return;

        Field f = Game.getFromField();

        if(this.isEmpty()){
            if(f == null)
                return;
            else{
                boolean isMoved = f.getPiece().move(f, this, Game.getBoard());
                if(isMoved) {
                    Game.nextTurn();
                    Game.setToField(null);
                    Game.setFromField(null);
                } else
                    Game.setFromField(null);
            }

        } else{
            if(f == null){
                if(this.getPiece().getColor() == Game.getOnTurn()) {
                    Game.setFromField(this);
                    Game.setFromField(this);
                }
            } else{
                boolean isAttacted = f.getPiece().move(f, this, Game.getBoard());
                if(isAttacted){
                    Game.nextTurn();
                    Game.setToField(null);
                    Game.setFromField(null);
                } else
                    Game.setFromField(null);
            }
        }
    }

    private void setButtonDesign() {

        if(color == Color.BLACK)
            this.setColor("black");
        else
            this.setColor("fffddd");


        view = new ImageView();
        view.setFitHeight(50);
        view.setFitWidth(50);
        this.setGraphic(view);

        if(!isEmpty())
            setImage();

    }

    private void setImage(){
        Image image = piece.getImage();
        this.view.imageProperty().set(image);
        this.setGraphic(view);
    }

    public void refreshField(){ // TODO: Use this in move
        if(this.isEmpty()){
            this.view.imageProperty().set(null);
        } else{
            setImage();
        }
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public boolean isEmpty(){
        return piece == null;
    }

    public Color getColor(){
        return color;
    }

    public void setColor(String color) {
        switch(color){
            case "black":
                color = "f0c29a";
                break;
            case "white":
                color = "fffddd";
                break;
        }
        this.setStyle("-fx-background-color: #" + color + "; ");
    }
}
